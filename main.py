# A program to check CPU and RAM usage
# Authors: Sabrina Zeidler, Tobias Haase

# All the imports
import psutil
import time
import sys
import alarm
import argparse
import configparser
import os


# Program will exit if the ini file is not available
if not (os.path.isfile("monitoring.ini")):
    print("Error: Configfile monitoring.ini is missing.")
    sys.exit()

# Define Configparser
config = configparser.ConfigParser()
config.read("monitoring.ini")
# Read Limits
cpuwarn = 20.0                                      # Example how to set a value for the check if there's no info in the ini file
if config.has_option("cpu","softlimit"):            # Ini file overwrites the set value if there's a corresponding line in the file
    cpuwarn = float(config["cpu"]["softlimit"])
cpucritical = float(config["cpu"]["hardlimit"])
ramwarn = float(config["ram"]["softlimit"])
ramcritical = float(config["ram"]["hardlimit"])
# Read Email Info
sender = config["mail"]["sender"]
receiver = config["mail"]["receiver"]



# Function for the menu
def menu():
    print("***Main Menu***")
    time.sleep(1)

    choice = input("""

    A: CPU
    B: RAM
    Q: Quit

    Please make a selection from A, B or Q.
    """)

    if choice == "A" or choice == "a":
        cpu()
    elif choice == "B" or choice == "b":
        ram()
    elif choice == "Q" or choice == "q":
        end()
    elif choice != "A, a, B, b, Q ,q":
        print("Please select one of the options above.")
        menu()


# Function to end the program
def end():
    time.sleep(2)
    sys.exit()


# Function for the CPU

def cpu():
    print("CPU Usage:")
    while True:
        cpu_usage = psutil.cpu_percent(interval=1)
        print(str(cpu_usage)+"%")
        alarm.cpu_alarm(cpu_usage, cpuwarn, cpucritical, sender, receiver)


# function for the ram
def ram():
    print("Ram Usage in %:")
    while True:
        ram_usage = psutil.virtual_memory().percent
        print(str(ram_usage)+"%")
        alarm.ram_alarm(ram_usage, ramwarn, ramcritical, sender, receiver)
        time.sleep(1)


# Creating argparse commands to start functions directly from console
if __name__ == "__main__":
    parser = argparse.ArgumentParser(add_help=False)

    parser.add_argument("-h", "--help", action="help", help="A simple program to check CPU and RAM."
                        " Use the parameters below to start the functions directly."
                        " If you use no parameters, you'll get the menu."
                        " You can use upper and lower case letters.")
    parser.add_argument("-c", "--CPU", help="Call the CPU Function", action="store_true")
    parser.add_argument("-r", "--RAM", help="Call the RAM Function", action="store_true")

    args = parser.parse_args()

    if args.CPU:
        cpu()
    if args.RAM:
        ram()


# Run the program if no parameters are used
menu()
