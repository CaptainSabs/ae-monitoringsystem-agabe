import psutil
import logging
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
import configparser


# Define the logging
logger = logging.getLogger(__name__)
# Write into a file, overwrite not active, can be put in with w instead of a
file_handler = logging.FileHandler("log.txt","a")
formatter = logging.Formatter("%(asctime)s : %(name)s : %(levelname)s - %(message)s")
file_handler.setFormatter(formatter)
logger.setLevel(logging.WARNING)
logger.addHandler(file_handler)
# Print into console
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
logger.addHandler(ch)
# Everything that's printed gets into a log
print = logger.info



# Variables
cpu_usage = psutil.cpu_percent(interval=1)
ram_usage = psutil.virtual_memory()


# Alarm for the CPU
def cpu_alarm(cpu_usage, cpuwarn, cpucritical, sender, receiver):
    if cpuwarn < cpu_usage < cpucritical:
        logger.warning("CPU - Close to the limit! " + str(cpu_usage)+ "%"+ " / " + str(cpucritical)+"%")
    elif cpu_usage >= cpucritical:
        logger.critical("CPU - Error! Critical Level! " + str(cpu_usage)+ "%"+ " / " + str(cpucritical)+"%")
        send_mail(sender, receiver)
        exit()


# Alarm for the memory
def ram_alarm(ram_usage,ramwarn, ramcritical, sender, receiver):
    if ramwarn < ram_usage < ramcritical:
        logger.warning("RAM - Close to the limit! " + str(ram_usage) + "%" + " / " + str(ramcritical)+"%")
    elif ram_usage >= ramcritical:
        logger.critical("RAM - Error! Critical Level! " + str(ram_usage) + "%" + " / " + str(ramcritical)+"%")
        send_mail(sender, receiver)
        exit()


#Function for the warning email when hard limit is reached
def send_mail(sender, receiver):
    # Emails are tested on the local DebuggingServer or MailHog
    # Terminal gets following command: sudo python3 -m smtpd -c DebuggingServer -n localhost:25
    # Setup MailHog in terminal after installing it with: ~/go/bin/MailHog web:0.0.0.0:8025 localhost 1025
    # Change localhost in code depending of what you want to use
    # DebuggingServer is preferred

    # The important stuff
    msg = MIMEMultipart()
    msg["From"] = sender
    msg["To"] = receiver
    msg["Subject"] = "Alarm"

    # Defining the body
    body = "There is an issue with the limit. Check the log:"
    msg.attach(MIMEText(body, "plain"))
    filename = "log.txt"

    # Attaching the file
    with open("log.txt", "rb") as attachment:
        part = MIMEBase("application", "octet-stream")
        part.add_header("Content-Disposition", "attachment; filename= %s" % filename) #Add header
        part.set_payload(attachment.read())

        msg.attach(part) # Attaching the file to the message and convert it to string
        text = msg.as_string()


    # Sending the email
    server = smtplib.SMTP("localhost", 25)
    server.sendmail(sender, receiver, text)
    server.quit()
