Monitoring System Mini Projekt von Sabrina Zeidler und Tobias Haase



Monitoring System welches die CPU und RAM Auslastung überprüft und anzeigt.

Der Main ist das Monitoring Modul zum Überprüfen der Auslastung.

Sobald Schwellenwerte erreicht werden, welche aus der Ini Datei ausgelesen werden, erfolgt eine Warnung und ggf eine Email.

Die überprüften Werte werden in der Konsole angezeigt aber nur die Warnungen in die Log Datei übernommen um einen überfüllten Log zu vermeiden.

Die Logfile erstellt sich bei der ersten Benutzung selbst und speichert alle Warnungen welche die Limits betreffen. Es wird nichts überschrieben.

Das Alarm Modul ist für Warnungen und Emails verantwortlich.

Configparser liest die Infos aus der monitoring.ini Datei aus. Ist diese nicht vorhanden, wird ein Fehler geworfen.

Email wird mit dem local DebuggingServer(localhost 25) oder MailHog(localhost 1025) getestet.

Argparse zum Steuern der Parameter.

